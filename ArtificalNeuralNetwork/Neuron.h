//
//  Neuron.h
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/8.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#ifndef __ArtificalNeuralNetwork__Neuron__
#define __ArtificalNeuralNetwork__Neuron__

#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <cassert>
using namespace std;

struct Connection
{
    double weight;
    double deltaWeight;
};

class Neuron;

typedef vector< Neuron > Layer;

class Neuron
{
public:
    Neuron( unsigned numOutputs, unsigned index );
    void setOutputVal( double val ) { myOutputVal = val; }
    double getOutputVal( void ) const { return myOutputVal; }
    void feedForward( const Layer &prevLayer );
    void calcOutputGradients( double targetVal );
    void calcHiddenGradients( const Layer &nextLayer );
    void updateInputWeights( Layer &prevLayer );
    
private:
    static double eta; // [ 0.0..1.0 ] overall net training rate
    static double alpha; // [ 0.0..n] mutipler of last weight change ( momentum )
    static double transferFunction( double x );
    static double transferFunctionDerivative( double x );
    static double randomWeight( void ) { return rand() / double( RAND_MAX ); }
    double sumDOW( const Layer &nextlayer ) const;
    double myOutputVal;
    vector< Connection > myOutputWeights;
    unsigned myIndex;
    double myGradient;
};

#endif /* defined(__ArtificalNeuralNetwork__Neuron__) */
