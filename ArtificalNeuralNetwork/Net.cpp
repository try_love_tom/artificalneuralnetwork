//
//  Net.cpp
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/8.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include "Net.h"

void Net::getResults( vector< double > &resaultVals ) const
{
    resaultVals.clear();
    
    for ( unsigned n = 0; n < myLayers.back().size() - 1; ++n )
    {
        resaultVals.push_back( myLayers.back()[ n ].getOutputVal() );
    }
}

void Net::backProp( const vector< double > &targetVals )
{
    // Calculate overall net error ( RMS of output neuron errors )
    Layer &outputLayer = myLayers.back();
    
    myError = 0.0;
    for ( unsigned n = 0; n < outputLayer.size() - 1; ++n )
    {
        double delta = targetVals[ n ] - outputLayer[ n ].getOutputVal();
        myError += delta * delta;
    }
    myError /= outputLayer.size() - 1; // get average error squared
    myError = sqrt( myError ); // RMS
    
    // Implement a recent average measurement:
    myRecentAverageError = ( myRecentAverageError * myRecentAverageSmoothingFactor + myError ) / ( myRecentAverageSmoothingFactor + 1.0 );
    
    // Calculate output layer gradients
    for ( unsigned n = 0; n < outputLayer.size() - 1; ++n )
    {
        outputLayer[ n ].calcOutputGradients( targetVals[ n ] );
    }
    
    // Calculate gradientss on hidden layers
    for ( unsigned layerNum = myLayers.size() - 2; layerNum >0; --layerNum )
    {
        Layer &hiddenLayer = myLayers[ layerNum ];
        Layer &nextLayer = myLayers[ layerNum + 1 ];
        
        for ( unsigned n = 0; n < hiddenLayer.size(); ++n )
        {
            hiddenLayer[ n ].calcHiddenGradients( nextLayer );
        }
    }
    
    // For all layers from outputs to first hidden layer,
    // oudate connection weights
    for ( unsigned layerNum = myLayers.size() - 1; layerNum > 0; -- layerNum )
    {
        Layer &layer = myLayers[ layerNum ];
        Layer &prevLayer = myLayers[ layerNum - 1 ];
        
        for ( unsigned n = 0; n < layer.size() - 1; ++n )
        {
            layer[ n ].updateInputWeights( prevLayer );
        }
    }
}

void Net::feedForward( const vector< double > &inputVals )
{
    assert( inputVals.size() == myLayers[ 0 ].size() - 1 );
    
    // Assign ( latch ) the input values into the input neurons
    for ( unsigned i = 0; i < inputVals.size(); ++i )
    {
        myLayers[ 0 ][ i ].setOutputVal( inputVals[ i ] );
    }
    
    // Forward propagate
    for ( unsigned layerNum = 1; layerNum < myLayers.size(); ++layerNum )
    {
        Layer &prevLayer = myLayers[ layerNum - 1 ];
        for ( unsigned n = 0; n < myLayers[ layerNum ].size() - 1; ++n )
        {
            myLayers[ layerNum ][ n ].feedForward( prevLayer );
        }
    }
}

Net::Net( const vector< unsigned > &topology )
{
    unsigned numLayers = topology.size();
    
    for ( unsigned layerNum = 0; layerNum < numLayers; ++layerNum )
    {
        myLayers.push_back( Layer() );
        unsigned numOutputs = ( ( layerNum == topology.size() - 1 ) ? ( 0 ) : ( topology[ layerNum + 1 ] ) );
        
        // We have made a new layer, now fill it with neurons, and
        // add a bias neuron to the layer:
        for ( unsigned neuronNum = 0; neuronNum <= topology[ layerNum ]; ++neuronNum )
        {
            myLayers.back().push_back( Neuron( numOutputs, neuronNum ) );
            cout << "Make a Neuron!" << endl;
        }
        // Force the bias node's output value to 1.0. It's the last neuron above
        myLayers.back().back().setOutputVal( 1.0 );
    }
}
