//
//  main.cpp
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/7.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include <iostream>
#include <vector>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iomanip>
using namespace std;

// Silly class to read training data from file
class TrainingData
{
public:
    TrainingData( const string fileName );
    bool isEof( void ) { return myTrainingDataFile.eof(); }
    void getTopology( vector< unsigned > &topology );
    
    // Returns the number of input values read form the file:
    unsigned getNextInputs( vector< double > &inputVals );
    unsigned getTargetOutputs( vector< double > &targetOutputVals );
    
private:
    ifstream myTrainingDataFile;
};

void TrainingData::getTopology( vector< unsigned > & topology )
{
    string line;
    string label;
    
    getline( myTrainingDataFile, line );
    stringstream ss( line );
    ss >> label;
    if ( this -> isEof() || label.compare( "topology:" ) != 0 )
    {
        abort();
    }
    while ( !ss.eof() )
    {
        unsigned n;
        ss >> n;
        topology.push_back( n );
    }
    return;
}

TrainingData::TrainingData( const string fileName )
{
    myTrainingDataFile.open( fileName.c_str() );
}

unsigned TrainingData::getNextInputs( vector< double > &inputVals )
{
    inputVals.clear();
    
    string line;
    getline( myTrainingDataFile, line );
    stringstream ss( line );
    
    string label;
    ss >> label;
    if ( label.compare( "in:" ) == 0 )
    {
        double oneValue;
        
        while ( ss >> oneValue )
        {
            inputVals.push_back( oneValue );
        }
    }
    return inputVals.size();
}

unsigned TrainingData::getTargetOutputs( vector< double > &targetOutputVals )
{
    targetOutputVals.clear();
    
    string line;
    getline( myTrainingDataFile, line );
    stringstream ss( line );
    
    string label;
    ss >> label;
    if ( label.compare("out:") == 0 )
    {
        double oneValue;
        
        while ( ss >> oneValue )
        {
            targetOutputVals.push_back( oneValue );
        }
    }
    return targetOutputVals.size();
}

struct Connection
{
    double weight;
    double deltaWeight;
};

class Neuron;

typedef vector< Neuron > Layer;

class Neuron
{
public:
    Neuron( unsigned numOutputs, unsigned index );
    Neuron( unsigned numOutputs, unsigned index, vector< Connection > &outputWeights );
    void setOutputVal( double val ) { myOutputVal = val; }
    double getOutputVal( void ) const { return myOutputVal; }
    void feedForward( const Layer &prevLayer );
    void calcOutputGradients( double targetVal );
    void calcHiddenGradients( const Layer &nextLayer );
    void updateInputWeights( Layer &prevLayer );
    vector< Connection > getMyOutputWeights( void ) { return myOutputWeights; }
    
private:
    static double eta; // [ 0.0..1.0 ] overall net training rate
    static double alpha; // [ 0.0..n] mutipler of last weight change ( momentum )
    static double transferFunction( double x );
    static double transferFunctionDerivative( double x );
    static double randomWeight( void ) { return rand() / double( RAND_MAX ); }
    double sumDOW( const Layer &nextlayer ) const;
    double myOutputVal;
    vector< Connection > myOutputWeights;
    unsigned myIndex;
    double myGradient;
};

void Neuron::updateInputWeights( Layer &prevLayer )
{
    // The weights to be updated are in the Connection container
    // in the neuron in the preceding layer
    for ( unsigned n = 0; n < prevLayer.size(); ++n )
    {
        Neuron &neuron = prevLayer[ n ];
        double oldDeltaWeight = neuron.myOutputWeights[ myIndex ].deltaWeight;
        // Individual input, magnified by the gradient and train rate:
        // Also add momentum = a fraction of the previous delta weight
        double newDeltaWeight = eta * neuron.getOutputVal() * myGradient + alpha * oldDeltaWeight;
        
        neuron.myOutputWeights[ myIndex ].deltaWeight = newDeltaWeight;
        neuron.myOutputWeights[ myIndex ].weight += newDeltaWeight;
    }
}

double Neuron::eta = 0.15; // overall net learning rate
double Neuron::alpha = 0.5; // momentum, nultiplier of last deltaWeight, [ 0.0..n ]

double Neuron::sumDOW( const Layer &nextLayer ) const
{
    double sum = 0.0;
    
    // Sum out contributons of errors at the nodes we feed
    for ( unsigned n = 0; n < nextLayer.size() - 1; ++n )
    {
        sum += myOutputWeights[ n ].weight * nextLayer[ n ].myGradient;
    }
    return sum;
}

void Neuron::calcHiddenGradients( const Layer &nextLayer )
{
    double dow = sumDOW( nextLayer );
    
    myGradient = dow * Neuron::transferFunctionDerivative( myOutputVal );
}

void Neuron::calcOutputGradients( double targetVal )
{
    double delta = targetVal - myOutputVal;
    
    myGradient = delta * Neuron::transferFunctionDerivative( myOutputVal );
}

double Neuron::transferFunction( double x )
{
    // tanh - output range [ -1.0..1.0 ]
    return tanh( x );
}

double Neuron::transferFunctionDerivative( double x )
{
    // tanh derivative
    return 1.0 - x * x;
}

void Neuron::feedForward( const Layer &prevLayer )
{
    double sum = 0.0;
    
    // Sum the previous layer's output ( which are our inputs )
    // Include the bias node form the previous layer.
    
    for ( unsigned n = 0; n < prevLayer.size(); ++n )
    {
        sum += prevLayer[ n ].getOutputVal() * prevLayer[ n ].myOutputWeights[ myIndex ].weight;
    }
    myOutputVal = Neuron::transferFunction( sum );
}

Neuron::Neuron( unsigned numOutputs, unsigned index )
{
    for ( unsigned c = 0; c < numOutputs; ++c )
    {
        myOutputWeights.push_back( Connection() );
        myOutputWeights.back().weight = randomWeight();
    }
    myIndex = index;
}

Neuron::Neuron( unsigned numOutputs, unsigned index, vector< Connection > &outputWeights )
{
    for ( unsigned c = 0; c < numOutputs; ++c )
    {
        myOutputWeights.push_back( outputWeights[ c ] );
    }
    myIndex = index;
}

class Net
{
public:
    Net( const vector< unsigned > &topology );
    Net( const string fileName );
    void feedForward( const vector< double > &inputVals );
    void backProp( const vector< double > &targetVals );
    void getResults( vector< double > &resaultVals ) const;
    double getRecentAverageError( void ) const { return myRecentAverageError; }
    void save( const string fileName );
    
private:
    vector< Layer > myLayers; // myLayers[ layerNum ][ neuronNum ]
    double myError;
    double myRecentAverageError;
    double myRecentAverageSmoothingFactor;
};

Net::Net( const string fileName )
{
    ifstream inputFile;
    vector< unsigned > topology;
    vector< Connection > outputWeights;
    string line;
    string label;
    
    inputFile.open( fileName.c_str() );
    getline( inputFile, line );
    stringstream ss( line );
    ss >> label;
    if ( inputFile.eof() || label.compare( "topology:" ) != 0 )
    {
        abort();
    }
    while ( !ss.eof() )
    {
        unsigned n;
        ss >> n;
        topology.push_back( n );
    }
    
    unsigned numLayers = topology.size();
    
    for ( unsigned layerNum = 0; layerNum < numLayers - 1; ++layerNum )
    {
        myLayers.push_back( Layer() );
        unsigned numOutputs = ( ( layerNum == topology.size() - 1 ) ? ( 0 ) : ( topology[ layerNum + 1 ] ) );
        getline( inputFile, line );
        outputWeights.clear();
        
        // We have made a new layer, now fill it with neurons, and
        // add a bias neuron to the layer:
        for ( unsigned neuronNum = 0; neuronNum <= topology[ layerNum ]; ++neuronNum )
        {
            for ( unsigned i = 0; i < 2; ++i )
            {
                getline( inputFile, line );
            }
            stringstream weightStringStream( line );
            
            weightStringStream >> label;
            if ( inputFile.eof() || label.compare( "weights:" ) != 0 )
            {
                abort();
            }
            outputWeights.clear();
            while ( !weightStringStream.eof() )
            {
                Connection outputWeight;
                weightStringStream >> outputWeight.weight;
                outputWeight.deltaWeight = outputWeight.weight;
                outputWeights.push_back( outputWeight );
            }
            myLayers.back().push_back( Neuron( numOutputs, neuronNum, outputWeights ) );
            cout << "Make a Neuron!" << endl;
        }
        // Force the bias node's output value to 1.0. It's the last neuron above
        myLayers.back().back().setOutputVal( 1.0 );
    }
    for ( unsigned layerNum = numLayers - 1; layerNum < numLayers; ++layerNum )
    {
        myLayers.push_back( Layer() );
        unsigned numOutputs = ( ( layerNum == topology.size() - 1 ) ? ( 0 ) : ( topology[ layerNum + 1 ] ) );
        
        // We have made a new layer, now fill it with neurons, and
        // add a bias neuron to the layer:
        for ( unsigned neuronNum = 0; neuronNum <= topology[ layerNum ]; ++neuronNum )
        {
            myLayers.back().push_back( Neuron( numOutputs, neuronNum ) );
            cout << "Make a Neuron!" << endl;
        }
        // Force the bias node's output value to 1.0. It's the last neuron above
        myLayers.back().back().setOutputVal( 1.0 );
    }
    inputFile.close();
}

void Net::save( const string fileName )
{
    fstream outputFile;
    
    outputFile.open( fileName, ios::out | ios::trunc );
    if ( !outputFile )
    {
        cout << "save error" << endl;
    }
    else
    {
        outputFile << "topology:";
        for ( unsigned layerNum = 0; layerNum < myLayers.size(); ++layerNum )
        {
            outputFile << " " << myLayers[ layerNum ].size() - 1;
        }
        outputFile << endl;
        for ( unsigned layerNum = 0; layerNum < myLayers.size() - 1; ++layerNum )
        {
            outputFile << "layer: " << layerNum << endl;
            for ( unsigned neuronNum = 0; neuronNum < myLayers[ layerNum ].size(); ++neuronNum )
            {
                outputFile << "neuron: " << neuronNum << endl;
                vector< Connection > outputWeights = myLayers[ layerNum ][ neuronNum ].getMyOutputWeights();
                
                outputFile << "weights: ";
                for ( unsigned index = 0; index < outputWeights.size(); ++index )
                {
                    outputFile << setprecision( 20 ) << outputWeights[ index ].weight << " ";
                }
                outputFile << endl;
            }
        }
    }
    outputFile.close();
}

void Net::getResults( vector< double > &resaultVals ) const
{
    resaultVals.clear();
    
    for ( unsigned n = 0; n < myLayers.back().size() - 1; ++n )
    {
        resaultVals.push_back( myLayers.back()[ n ].getOutputVal() );
    }
}

void Net::backProp( const vector< double > &targetVals )
{
    // Calculate overall net error ( RMS of output neuron errors )
    Layer &outputLayer = myLayers.back();
    
    myError = 0.0;
    for ( unsigned n = 0; n < outputLayer.size() - 1; ++n )
    {
        double delta = targetVals[ n ] - outputLayer[ n ].getOutputVal();
        myError += delta * delta;
    }
    myError /= outputLayer.size() - 1; // get average error squared
    myError = sqrt( myError ); // RMS
    
    // Implement a recent average measurement:
    myRecentAverageError = ( myRecentAverageError * myRecentAverageSmoothingFactor + myError ) / ( myRecentAverageSmoothingFactor + 1.0 );
    
    // Calculate output layer gradients
    for ( unsigned n = 0; n < outputLayer.size() - 1; ++n )
    {
        outputLayer[ n ].calcOutputGradients( targetVals[ n ] );
    }
    
    // Calculate gradientss on hidden layers
    for ( unsigned layerNum = myLayers.size() - 2; layerNum > 0; --layerNum )
    {
        Layer &hiddenLayer = myLayers[ layerNum ];
        Layer &nextLayer = myLayers[ layerNum + 1 ];
        
        for ( unsigned n = 0; n < hiddenLayer.size(); ++n )
        {
            hiddenLayer[ n ].calcHiddenGradients( nextLayer );
        }
    }
    
    // For all layers from outputs to first hidden layer,
    // oudate connection weights
    for ( unsigned layerNum = myLayers.size() - 1; layerNum > 0; --layerNum )
    {
        Layer &layer = myLayers[ layerNum ];
        Layer &prevLayer = myLayers[ layerNum - 1 ];
        
        for ( unsigned n = 0; n < layer.size() - 1; ++n )
        {
            layer[ n ].updateInputWeights( prevLayer );
        }
    }
}

void Net::feedForward( const vector< double > &inputVals )
{
    assert( inputVals.size() == myLayers[ 0 ].size() - 1 );
    
    // Assign ( latch ) the input values into the input neurons
    for ( unsigned i = 0; i < inputVals.size(); ++i )
    {
        myLayers[ 0 ][ i ].setOutputVal( inputVals[ i ] );
    }
    
    // Forward propagate
    for ( unsigned layerNum = 1; layerNum < myLayers.size(); ++layerNum )
    {
        Layer &prevLayer = myLayers[ layerNum - 1 ];
        for ( unsigned n = 0; n < myLayers[ layerNum ].size() - 1; ++n )
        {
            myLayers[ layerNum ][ n ].feedForward( prevLayer );
        }
    }
}

Net::Net( const vector< unsigned > &topology )
{
    unsigned numLayers = topology.size();
    
    for ( unsigned layerNum = 0; layerNum < numLayers; ++layerNum )
    {
        myLayers.push_back( Layer() );
        unsigned numOutputs = ( ( layerNum == topology.size() - 1 ) ? ( 0 ) : ( topology[ layerNum + 1 ] ) );
        
        // We have made a new layer, now fill it with neurons, and
        // add a bias neuron to the layer:
        for ( unsigned neuronNum = 0; neuronNum <= topology[ layerNum ]; ++neuronNum )
        {
            myLayers.back().push_back( Neuron( numOutputs, neuronNum ) );
            cout << "Make a Neuron!" << endl;
        }
        // Force the bias node's output value to 1.0. It's the last neuron above
        myLayers.back().back().setOutputVal( 1.0 );
    }
}

void showVectorVals( string label, vector< double > &v )
{
    cout << label << " ";
    for ( unsigned i = 0; i < v.size(); ++i )
    {
        cout << v[ i ] << " ";
    }
    cout << endl;
}

int main()
{
    TrainingData trainData( "/tmp/trainingData.txt" );
    
    vector< unsigned > topology;
    trainData.getTopology( topology );
    Net myNet( topology );
    
    vector< double > inputVals, targetVals, resultVals;
    int trainingPass = 0;
    
    while ( !trainData.isEof() )
    {
        ++trainingPass;
        cout << endl << "Pass " << trainingPass;
        
        // Get new intput data and feed it forward:
        if ( trainData.getNextInputs( inputVals ) != topology[ 0 ] )
        {
            break;
        }
        showVectorVals( "\nInputs: ", inputVals );
        myNet.feedForward( inputVals );
        
        // Collect the net's actural results:
        myNet.getResults( resultVals );
        showVectorVals( "Output: ", resultVals );
        
        // Train the net what the outputs should have been:
        trainData.getTargetOutputs( targetVals );
        showVectorVals( "Targets: ", targetVals );
        assert( targetVals.size() == topology.back());
        
        myNet.backProp( targetVals );
        
        // Report how well the training is working, average over recent
        cout << "Net recent average error: " << myNet.getRecentAverageError() << endl;
    }
    myNet.save( "/tmp/trainingTemp.txt" );
    cout << endl << "save Done" << endl;
    
    Net clone( "/tmp/trainingTemp.txt" );
    clone.save( "/tmp/clone.txt" );
    
    while ( true )
    {
        double a, b;
        
        a = 0, b = 0;
        inputVals.clear();
        cout << "\nInput: ";
        cin >> a >> b;
        inputVals.push_back( a );
        inputVals.push_back( b );
        if ( inputVals.size() != topology[ 0 ] )
        {
            printf( "error" );
        }
        clone.feedForward( inputVals );
        
        // Collect the net's actural results:
        clone.getResults( resultVals );
        showVectorVals( "Output: ", resultVals );
    }
    
    cout << endl << "Done" << endl;
    return 0;
}
