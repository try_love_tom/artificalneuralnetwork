//
//  Net.h
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/8.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#ifndef __ArtificalNeuralNetwork__Net__
#define __ArtificalNeuralNetwork__Net__

#include "Neuron.h"
using namespace std;

class Net
{
public:
    Net( const vector< unsigned > &topology );
    void feedForward( const vector< double > &inputVals );
    void backProp( const vector< double > &targetVals );
    void getResults( vector< double > &resaultVals ) const;
    double getRecentAverageError( void ) const { return myRecentAverageError; }
    
private:
    vector< Layer > myLayers; // myLayers[ layerNum ][ neuronNum ]
    double myError;
    double myRecentAverageError;
    double myRecentAverageSmoothingFactor;
};

#endif /* defined(__ArtificalNeuralNetwork__Net__) */
