//
//  trainingDataMaker.cpp
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/8.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

int main()
{
    // Random training sets for XOR -- two input and one output
    
    cout << "topology: 3 5 3" << endl;
    for ( int i = 1000; i >= 0; --i )
    {
        int n = ( int )( 3.0 * rand() / double( RAND_MAX ) );
        // user paper seaser stone
        int n1, n2, n3;
        // result paper seaser stone
        int t1, t2, t3;
        
        switch ( n )
        {
            case 0:
                n1 = 1.0;
                n2 = 0.0;
                n3 = 0.0;
                t1 = 0.0;
                t2 = 1.0;
                t3 = 0.0;
                break;
            case 1:
                n1 = 0.0;
                n2 = 1.0;
                n3 = 0.0;
                t1 = 0.0;
                t2 = 0.0;
                t3 = 1.0;
                break;
            case 2:
                n1 = 0.0;
                n2 = 0.0;
                n3 = 1.0;
                t1 = 1.0;
                t2 = 0.0;
                t3 = 0.0;
                break;
            default:
                break;
        }
        /*
        int n1 = ( int )( 2.0 * rand() / double( RAND_MAX ) );
        int n2 = ( int )( 2.0 * rand() / double( RAND_MAX ) );
        int t1 = n1 ^ n2; // should be 0 or 1
        int t2 = n1 & n2;*/
        cout << "in: " << n1 << ".0 " << n2 << ".0 " << n3 << ".0 " << endl;
        cout << "out: " << t1 << ".0 " << t2 << ".0 " << t3 << ".0 " << endl;
    }
}