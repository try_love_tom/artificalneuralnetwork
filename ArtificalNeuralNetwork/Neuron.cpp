//
//  Neuron.cpp
//  ArtificalNeuralNetwork
//
//  Created by 張子晏 on 2015/6/8.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include "Neuron.h"

void Neuron::updateInputWeights( Layer &prevLayer )
{
    // The weights to be updated are in the Connection container
    // in the neuron in the preceding layer
    for ( unsigned n = 0; n < prevLayer.size(); ++n )
    {
        Neuron &neuron = prevLayer[ n ];
        double oldDeltaWeight = neuron.myOutputWeights[ myIndex ].deltaWeight;
        // Individual input, magnified by the gradient and train rate:
        // Also add momentum = a fraction of the previous delta weight
        double newDeltaWeight = eta * neuron.getOutputVal() * myGradient + alpha * oldDeltaWeight;
        
        neuron.myOutputWeights[ myIndex ].deltaWeight = newDeltaWeight;
        neuron.myOutputWeights[ myIndex ].weight += newDeltaWeight;
    }
}

double Neuron::eta = 0.15; // overall net learning rate
double Neuron::alpha = 0.5; // momentum, nultiplier of last deltaWeight, [ 0.0..n ]

double Neuron::sumDOW( const Layer &nextLayer ) const
{
    double sum = 0.0;
    
    // Sum out contributons of errors at the nodes we feed
    for ( unsigned n = 0; n < nextLayer.size() - 1; ++n )
    {
        sum += myOutputWeights[ n ].weight * nextLayer[ n ].myGradient;
    }
    return sum;
}

void Neuron::calcHiddenGradients( const Layer &nextLayer )
{
    double dow = sumDOW( nextLayer );
    
    myGradient = dow * Neuron::transferFunctionDerivative( myOutputVal );
}

void Neuron::calcOutputGradients( double targetVal )
{
    double delta = targetVal - myOutputVal;
    
    myGradient = delta * Neuron::transferFunctionDerivative( myOutputVal );
}

double Neuron::transferFunction( double x )
{
    // tanh - output range [ -1.0..1.0 ]
    return tanh( x );
}

double Neuron::transferFunctionDerivative( double x )
{
    // tanh derivative
    return 1.0 - x * x;
}

void Neuron::feedForward( const Layer &prevLayer )
{
    double sum = 0.0;
    
    // Sum the previous layer's output ( which are our inputs )
    // Include the bias node form the previous layer.
    
    for ( unsigned n = 0; n < prevLayer.size(); ++n )
    {
        sum += prevLayer[ n ].getOutputVal() * prevLayer[ n ].myOutputWeights[ myIndex ].weight;
    }
    myOutputVal = Neuron::transferFunction( sum );
}

Neuron::Neuron( unsigned numOutputs, unsigned index )
{
    for ( unsigned c = 0; c < numOutputs; ++c )
    {
        myOutputWeights.push_back( Connection() );
        myOutputWeights.back().weight = randomWeight();
    }
    myIndex = index;
}
